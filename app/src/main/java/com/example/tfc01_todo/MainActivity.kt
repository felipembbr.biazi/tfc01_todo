package com.example.tfc01_todo

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private var etxtInput: EditText? = null
    private var etxtOutput: TextView? = null
    private var stwUrgent: Switch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.etxtInput = findViewById(R.id.etxtInput)
        this.etxtOutput = findViewById(R.id.etxtOutput)
        this.stwUrgent = findViewById(R.id.stwUrgent)
    }
    fun OnClickOk (v:View){
        var tarefa: String = this.etxtInput?.text.toString()
        if (tarefa.isNotEmpty()){
            val urgente: Boolean = this.stwUrgent?.isChecked == true
            tarefa = (if (urgente) "(${getString(R.string.urgent)}):"
                    else "(${getString(R.string.not)} ${getString(R.string.urgent)}):") + "${tarefa}\n"
            this.etxtOutput?.append(tarefa)
            this.etxtInput?.setText("")
            this.stwUrgent?.isChecked = false
        }
    }
}


